package com.template;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.StateAndContract;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import org.isda.cdm.Event;

import static com.template.TemplateContract.TEMPLATE_CONTRACT_ID;


@InitiatingFlow
@StartableByRPC
public class CDSIndexSwapFlow extends FlowLogic<SignedTransaction> {

    private CDSIndexSwapsState state;
    private  Party otherParty;

    public CDSIndexSwapFlow(CDSIndexSwapsState state) {
        this.state = state;
    }

    /**
     * The progress tracker provides checkpoints indicating the progress of the flow to observers.
     */

    private final ProgressTracker progressTracker = new ProgressTracker();

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }



    @Override
    @Suspendable
    public SignedTransaction call() throws FlowException {
        // We retrieve the notary identity from the network map.
        System.out.println("Initiating the flow");
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        CommandData cmdType = new TemplateContract.Commands.Action();
        Command cmd = new Command<>(cmdType, getOurIdentity().getOwningKey());

        // We create a transaction builder and add the components.
        final TransactionBuilder txBuilder = new TransactionBuilder(notary)
                .addOutputState(state, TEMPLATE_CONTRACT_ID)
                .addCommand(cmd);

        System.out.println("Build transaction ......");
        // Signing the transaction.
        final SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);

        System.out.println("Transaction Signed!!!!!");
        // Finalising the transaction.
        subFlow(new FinalityFlow(signedTx));

        return null;

    }
}
