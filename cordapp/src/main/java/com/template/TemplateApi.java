package com.template;

import com.google.common.collect.ImmutableList;
import com.regnosys.rosetta.common.serialisation.RosettaObjectMapper;
import net.corda.core.identity.Party;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.SignedTransaction;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.isda.cdm.Event;


import java.util.Collections;
import java.util.List;
import java.util.Set;

import static javax.ws.rs.core.Response.Status.CREATED;

// This API is accessible from /api/template. The endpoint paths specified below are relative to it.
@Path("template")
public class TemplateApi {
    private final CordaRPCOps rpcOps;

    public TemplateApi(CordaRPCOps services) {
        this.rpcOps = services;
    }

    /**
     * Accessible at /api/template/templateGetEndpoint.
     */
    @GET
    @Path("templateGetEndpoint")
    @Produces(MediaType.APPLICATION_JSON)
    public Response templateGetEndpoint() {
        return Response.ok("Template GET endpoint.").build();
    }

    @GET
    @Path("states")
    @Produces(MediaType.APPLICATION_JSON)
public Response templateGetEndpoint(@QueryParam("id") String id) {
        QueryCriteria.LinearStateQueryCriteria criteria = new QueryCriteria.LinearStateQueryCriteria(null, null, ImmutableList.of(id));
        CDSIndexSwapsState state = rpcOps.vaultQueryBy(criteria, null, null, CDSIndexSwapsState.class).getStates().get(0).getState().getData();
        return Response.ok("Template GET endpoint.").entity(state).build();
    }

    @POST
    @Path("createIndexCds")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createAnIndexCDS(String payload) {

        System.out.println("End point invoke !!!!!");
        String msg = "";
        try {

            Event event = RosettaObjectMapper.getDefaultRosettaObjectMapper().readValue(payload,Event.class);
           System.out.println(rpcOps.nodeInfo().getLegalIdentities());

           String lei1 = event.getParty().get(0).getLegalEntity().getEntityId();
           String lei2 = event.getParty().get(1).getLegalEntity().getEntityId();
           System.out.println(lei1);
           System.out.println(lei2);


            Set<Party> party1= rpcOps.partiesFromName(lei1,true);
            Set<Party> party2 = rpcOps.partiesFromName(lei2,true);

            System.out.println(party1);
            System.out.println(party2);


            CDSIndexSwapsState state = CDSIndexSwapStateMaker.createCDSIndexSwapState(event,party1.iterator().next(),party2.iterator().next());
            rpcOps.startFlowDynamic(CDSIndexSwapFlow.class, state).getReturnValue().get();

            System.out.println("All done");
        }
        catch(Exception ex){
            msg="something has gone wrong";
            ex.printStackTrace();
        }
        finally {
            return Response.status(CREATED).entity("Transaction Created").build();
        }
    }
}