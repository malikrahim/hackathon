package com.template.model.cds;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.corda.core.serialization.CordaSerializable;

import java.time.LocalDate;

@CordaSerializable
@AllArgsConstructor
@Getter
public class TradeDetails {
    private String productQualifier;
    private LocalDate tradeDate;
}
