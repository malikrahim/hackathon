package com.template.model.cds;

import lombok.Builder;
import lombok.Getter;
import net.corda.core.serialization.CordaSerializable;

import java.math.BigDecimal;
import java.time.LocalDate;

@CordaSerializable
@Builder
@Getter
public class InterestRatePayout {
        LocalDate stateDate;
        LocalDate endDate;
        double interestRate;
        String dayCountFraction;
}
