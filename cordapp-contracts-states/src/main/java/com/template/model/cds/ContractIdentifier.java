package com.template.model.cds;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
@Builder
@Getter
public class ContractIdentifier {
    private String identifier;
    private int version;
}