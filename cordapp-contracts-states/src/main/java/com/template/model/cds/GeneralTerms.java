package com.template.model.cds;


import lombok.Builder;
import lombok.Getter;
import net.corda.core.identity.Party;
import net.corda.core.serialization.CordaSerializable;
import org.isda.cdm.BusinessCenterEnum;
import org.isda.cdm.BusinessDayConventionEnum;

import java.util.List;

@CordaSerializable
@Builder
@Getter
public class GeneralTerms {
    Party buyer;
    Party seller;
    List<BusinessCenterEnum> businessCenter;
    BusinessDayConventionEnum businessDayConvention;
    String indexName;
    Integer indexSeries;
}
