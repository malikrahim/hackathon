package com.template.model.cds;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
@AllArgsConstructor
@Getter
public class PremiumFee {
    private double amount;
    private String currency;
}