package com.template;

import com.google.common.collect.ImmutableList;
import com.template.model.cds.ContractIdentifier;
import com.template.model.cds.GeneralTerms;
import com.template.model.cds.PremiumFee;
import com.template.model.cds.TradeDetails;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.Party;
import org.isda.cdm.*;

import java.util.UUID;


public class CDSIndexSwapStateMaker {

    public static CDSIndexSwapsState createCDSIndexSwapState(Event cdmevent, Party party1, Party party2 ) {

        Contract contract = cdmevent.getPrimitive().getNewTrade().get(0).getContract();

        org.isda.cdm.ContractIdentifier contractIdentifier = contract.getContractIdentifier().get(0);

        //build contract identifier;
        String eventIdentifier = contractIdentifier.getIdentifierValue().getIdentifier();
        int version = contractIdentifier.getVersion();

        System.out.println("***********************************"+eventIdentifier);

        ContractIdentifier identifierModel = ContractIdentifier.builder().identifier(eventIdentifier).version(version).build();


        ContractualProduct product = contract.getContractualProduct();
        org.isda.cdm.GeneralTerms terms = product.getEconomicTerms().getPayout().getCreditDefaultPayout().getGeneralTerms();

        Party buyer = terms.getBuyerSeller().getBuyerPartyReference().equals(party1.getName())?party1:party2;
        Party seller = terms.getBuyerSeller().getBuyerPartyReference().equals(party2.getName())?party2:party1;


        GeneralTerms generalTermsModel = GeneralTerms.builder().businessCenter(terms.getDateAdjustments().getBusinessCenters().getBusinessCenter()).
                businessDayConvention(terms.getDateAdjustments().getBusinessDayConvention()).
                buyer(buyer).
                seller(seller).
                indexName(terms.getIndexReferenceInformation().getIndexName()).
                indexSeries(terms.getIndexReferenceInformation().getIndexSeries()).
                build();


        InterestRatePayout interestRatePayout = product.getEconomicTerms().getPayout().getInterestRatePayout().get(0);

        com.template.model.cds.InterestRatePayout interestRatePayoutModel = com.template.model.cds.InterestRatePayout.builder().
                stateDate(interestRatePayout.getCalculationPeriodDates().getEffectiveDate().getAdjustableDate().getUnadjustedDate()).
                endDate(interestRatePayout.getCalculationPeriodDates().getTerminationDate().getUnadjustedDate()).
                interestRate(interestRatePayout.getInterestRate().getFixedRate().getInitialValue().doubleValue()).
                dayCountFraction(interestRatePayout.getDayCountFraction().toString()).
                build();

        Money feeValue = product.getEconomicTerms().getPayout().getCashflow().get(0).getCashflowAmount();

        PremiumFee feeModel = new PremiumFee(feeValue.getAmount().doubleValue(),feeValue.getCurrency());

        ProtectionTerms proytectionTerms = product.getEconomicTerms().getPayout().getCreditDefaultPayout().getProtectionTerms();
        com.template.model.cds.ProtectionTerms protectionTermsModel = new com.template.model.cds.ProtectionTerms(proytectionTerms.getNotionalAmount().getAmount().doubleValue(),proytectionTerms.getNotionalAmount().getCurrency());



        TradeDetails detailsModel = new TradeDetails(product.getProductIdentification().getProductQualifier(),contract.getTradeDate().getAdjustableDate().getUnadjustedDate());

        return CDSIndexSwapsState.builder().identifier(identifierModel).details(detailsModel).fee(feeModel).generalTerms(generalTermsModel).
                interestRatePayout(interestRatePayoutModel).terms(protectionTermsModel).parties(ImmutableList.of(party1,party2)).
                uniqueIdentifier(new UniqueIdentifier(identifierModel.getIdentifier(), UUID.randomUUID())).
                build();
    }

}
