package com.template;


import com.template.model.cds.*;
import lombok.Builder;
import lombok.Getter;

import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;


import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;


@Builder
@Getter
public class CDSIndexSwapsState implements LinearState {
    List<AbstractParty> parties;
    ContractIdentifier identifier;
    UniqueIdentifier uniqueIdentifier;
    GeneralTerms generalTerms;
    InterestRatePayout interestRatePayout;
    PremiumFee fee;
    ProtectionTerms terms;
    TradeDetails details;

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return parties;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return uniqueIdentifier;
    }
}
